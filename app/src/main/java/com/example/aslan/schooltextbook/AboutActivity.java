package com.example.aslan.schooltextbook;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.provider.SyncStateContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class AboutActivity extends AppCompatActivity {

    private String mBackgroundFilename;
    private View mBackgroundContainer;
    private ImageView mBlurImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        mBackgroundContainer = findViewById(R.id.container);
        mBlurImage = (ImageView) findViewById(R.id.blur_image);
        View aboutInfoTextView = findViewById(R.id.aboutInfoTextView);

        setBlurBackground();
        // mFigure.setBackgroundResource(getIntent().getIntExtra(SyncStateContract.Constants.DROID_ID, R.drawable.chocolate));
    }

    private void setBlurBackground() {
        mBackgroundFilename = getIntent().getStringExtra(Konst.BLUR_FILENAME);
        if(!TextUtils.isEmpty(mBackgroundFilename)){
            mBackgroundContainer.setVisibility(View.VISIBLE);
            Bitmap background = Utils.loadBitmapFromFile(mBackgroundFilename);
            if (background != null) {
                mBlurImage.setImageBitmap(background);
                mBlurImage.animate().alpha(1).setDuration(1000);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cleanupBlurBackground();
    }

    private void cleanupBlurBackground() {
        if(!TextUtils.isEmpty(mBackgroundFilename)){
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    Utils.deleteFile(mBackgroundFilename);
                    return null;
                }
            }.execute();
        }
    }
}
