package com.example.aslan.schooltextbook;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;

import org.apache.commons.codec.digest.DigestUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by sst on 6/24/16.
 */
public class Utils {
    private static final String TAG = "[Utls]";
    static String []vowels = {"a","e","i","o","u","y","а","е","о","ы","у","и","э","ю","я","іәұүө"};
    static Set<String> v;
    static {
        v = new TreeSet<String>();
        for ( String s: vowels )
            for ( Character c: s.toCharArray() )
                v.add(""+c);
    };
    public static String preprocess( String s, int width ) {
        StringBuffer sb = new StringBuffer();
        String []w = (s.replace("\n"," ")).split(" ");
        int i,j,k,n = w.length,t,l;
        for ( i = 0; i < n; i = j, sb.append("\n") ) {
            for ( k = 0, j = i; j < n; ) {
                if ( k+(l=w[j].length()) >= width ) break;
                k += l; ++j;
            }
            for ( t = i; t < j; sb.append(w[t++]+" ") );
        }
        return sb.toString();
    };

    public static Bitmap loadBitmapFromFile(String mBackgroundFilename) {
        BitmapFactory.Options localOptions = new BitmapFactory.Options();
        localOptions.inPurgeable = true;
        localOptions.inInputShareable = true;
        return BitmapFactory.decodeFile(mBackgroundFilename, localOptions);
    }

    public static String saveBitmapToFile(Activity context, Bitmap bitmap) {
        String filePath = "";
        try {
            File file = generateFile(context);
            file.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
            filePath = file.getAbsolutePath();
            return filePath;
        } catch (IOException exception) {}
        return filePath;
    }

    private static File generateFile(Activity context) {
        String timestamp = String.valueOf(System.currentTimeMillis());
        String md5 = String.valueOf(DigestUtils.md5(timestamp));
        File localFile = getCacheDir(context, "img_cache");
        if (!localFile.exists())
            localFile.mkdirs();
        return new File(localFile, md5);
    }

    private static File getCacheDir(Context context, String dirName) {
        return new File(context.getCacheDir(), dirName);
    }

    //http://stackoverflow.com/a/9596132/1121509
    public static Bitmap drawViewToBitmap(View view, int color) {
        if ( view == null )
            Log.d(TAG,"view == null");
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable =view.getBackground();
        if (bgDrawable!=null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(color);
        view.draw(canvas);
        return returnedBitmap;
    }

    public static boolean deleteFile(String filename) {
        return new File(filename).delete();
    }
}

