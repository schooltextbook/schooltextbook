package com.example.aslan.schooltextbook;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;
import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;


public class BookDetailsActivity extends AppCompatActivity {

    private static final String TAG = "BookDetails";
    private TextView authorsTextView;
    //private TextView nameTextView;
    private ImageView coverImageView;
    private TextView publisherPrice;
    private TextView publisherQuantity;
    private TextView publisherOfferTextView;
    private TextView contingentTextView;
    private TextView languageTextView;
    private TextView publishedAt;
    private TextView publisherTextView;
    private TextView nameTextView;

    private static final String ATTRIBUTE_TEXT[]  = {"Форма обучения: ", "Издательство: ", "Год выпуска: ", "Год обучения: ", "Цена: ", "Количество: "};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_details);

        authorsTextView = (TextView) findViewById(R.id.authorsTextView);
        nameTextView = (TextView) findViewById(R.id.nameTextView);
        publisherOfferTextView = (TextView) findViewById(R.id.publisherOfferTextView);
        publisherPrice = (TextView) findViewById(R.id.publisherPriceTextView);
        contingentTextView = (TextView) findViewById(R.id.contingentTextView);
        languageTextView = (TextView) findViewById(R.id.languageTextView);
        publisherQuantity = (TextView) findViewById(R.id.publisherQuantityTextView);
        coverImageView = (ImageView) findViewById(R.id.coverImageView);
        publishedAt = (TextView) findViewById(R.id.publishedAtTextView);
        publisherTextView = (TextView) findViewById(R.id.publisherTextView);

        String bookID = getIntent().getExtras().getString("ID");
        Log.d(TAG, bookID);

        downloadBookDetails(bookID);

    }

    private void downloadBookDetails(String objectID) {

        Backendless.Persistence.of(Books.class).findById(objectID, new AsyncCallback<Books>() {

            @Override
            public void handleResponse(Books response) {
                //Log.d(TAG, response.toString());
                displayBookDetails(response);
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.e(TAG, "failed to download book details  " + fault.getMessage());
            }

        });

        /*Backendless.Persistence.of(Books.class).find(query, new AsyncCallback<BackendlessCollection<Books>>() {
            @Override
            public void handleResponse(BackendlessCollection<Books> response) {

                List<Books> book = response.getData();

                Log.d(TAG, book.get(0).toString());
                //displayBookDetails(response.getData());
            }

            @Override
            public void handleFault(BackendlessFault fault) {

                Log.e(TAG, "failed to download book details");

            }
        });*/
    }

    private void displayBookDetails(Books book) {

        //Log.d(TAG, "books details after dowload: " + book.toString());

        Spannable languageText = new SpannableString(ATTRIBUTE_TEXT[0] + book.getLanguage());
        Spannable publisherText = new SpannableString(ATTRIBUTE_TEXT[1] + book.getPublisher());
        Spannable publishedAtText = new SpannableString(ATTRIBUTE_TEXT[2] + book.getPublished_at());
        Spannable contingentText = new SpannableString(ATTRIBUTE_TEXT[3] + book.getContingent());
        Spannable publisherPriceText = new SpannableString(ATTRIBUTE_TEXT[4] + (book.getPublisher_price() != null ? book.getPublisher_price() + " KZT" : " KZT"));
        Spannable publisherQuantityText = new SpannableString(ATTRIBUTE_TEXT[5] + (book.getPublisher_quantity() != null ? book.getPublisher_quantity() + " шт.": " нет в наличии"));

        languageText.setSpan(new StyleSpan(Typeface.BOLD), 0 , languageText.toString().indexOf(":"), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        publisherText.setSpan(new StyleSpan(Typeface.BOLD), 0, publisherText.toString().indexOf(":"), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        publishedAtText.setSpan(new StyleSpan(Typeface.BOLD), 0, publishedAtText.toString().indexOf(":"), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        contingentText.setSpan(new StyleSpan(Typeface.BOLD), 0, contingentText.toString().indexOf(":"), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        publisherPriceText.setSpan(new StyleSpan(Typeface.BOLD), 0, publisherPriceText.toString().indexOf(":"), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        publisherQuantityText.setSpan(new StyleSpan(Typeface.BOLD), 0, publisherQuantityText.toString().indexOf(":"), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        authorsTextView.setText( book.getAuthors() );

        nameTextView.setText( book.getName() );

        publisherQuantity.setText( publisherQuantityText );
        publisherPrice.setText( publisherPriceText );

        publisherOfferTextView.setText( publisherText );
        contingentTextView.setText( contingentText );
        languageTextView.setText( languageText );
        publishedAt.setText( publishedAtText);
        publisherTextView.setText( publisherText );

        String coverImageUrl = book.getCover_image();
        Glide.with(this).load(coverImageUrl).into( coverImageView );
    }
}
