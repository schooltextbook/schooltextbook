package com.example.aslan.schooltextbook;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;

import java.util.List;
import java.util.Locale;

import jp.wasabeef.blurry.Blurry;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "[MainActivity]";
    private Button activityOneButton, activityTwoButton;
    private TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        (activityOneButton = (Button) findViewById(R.id.activityOneButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onActivityOneButtonClick();
            }
        });
        (activityTwoButton = (Button) findViewById(R.id.activityTwoButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,AboutActivity.class);
                Log.d(TAG,"container = "+R.id.container);
                new BlurTask(MainActivity.this,i,R.id.mainLinearLayout).execute();
            }
        });
        Typeface typeface = Typeface.createFromAsset(getAssets(),"Antiqua.ttf");
        tv = (TextView) findViewById(R.id.tit);
        tv.setTypeface(typeface);
        activityOneButton.setTypeface(typeface);
        activityTwoButton.setTypeface(typeface);

        Backendless.initApp(this,Konst.APP_ID,Konst.CLIENT_KEY,Konst.VERSION);
    }

    private void onActivityOneButtonClick() {
        startActivity(new Intent(this,ListOfBooksActivity.class));
    }

    private void startLisfOfBooksActivity() {
        Intent intent = new Intent(this, ListOfBooksActivity.class);
        startActivity(intent);
    }
}

