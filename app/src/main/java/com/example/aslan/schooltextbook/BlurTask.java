package com.example.aslan.schooltextbook;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import jp.wasabeef.blurry.Blurry;

/**
 * Created by sst on 6/30/16.
 */
public class BlurTask extends AsyncTask<Void,Void,Void> {
    private static final String TAG = "[BlurTask]";
    private Activity mContext;
    private int mBlurLayoutId;
    private Bitmap mDownScaled;
    private Intent mIntent;

    public BlurTask( Activity activity, Intent intent, int layoutId ) {
        mContext = activity;
        mIntent = intent;
        mBlurLayoutId = layoutId;
        Log.d(TAG,"layoutId = "+mBlurLayoutId);
    }

    private View getViewFrame() {
        if ( mContext == null )
            Log.d(TAG,"mContext == null");
        else Log.d(TAG,"mContext != null");
        View v = mContext.findViewById(this.mBlurLayoutId);
        if ( v == null )
            Log.d(TAG,"view == null");
        return v;
    }

    @Override
    protected Void doInBackground(Void... params) {
        mIntent.putExtra(Konst.BLUR_FILENAME, getBlurredBackgroundFilename());
        return null;
    }

    @Override
    protected void onPreExecute() {
        this.mDownScaled = Utils.drawViewToBitmap(getViewFrame(),Color.parseColor("#fff5f5f5"));
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        mContext.startActivity(mIntent);
    }

    private String getBlurredBackgroundFilename() {
        Bitmap localBitmap = Blur.fastblur(this.mContext,this.mDownScaled,16);
        String str = Utils.saveBitmapToFile(this.mContext,localBitmap);
        this.mDownScaled.recycle();
        localBitmap.recycle();
        return str;
    }
}

