package com.example.aslan.schooltextbook;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by aslan on 25.06.2016.
 */
public class BookTextView extends TextView {

    public BookTextView(Context context, AttributeSet attrs) {

        super(context, attrs);
        this.setTextColor( Color.parseColor("#40031d") );

    }

}
